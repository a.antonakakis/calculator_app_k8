from flask import Flask
app = Flask(__name__)
port = 8081

in_memory_datastore = {
   "COBOL" : {"name": "COBOL", "publication_year": 1960, "contribution": "record data"},
   "ALGOL" : {"name": "ALGOL", "publication_year": 1958, "contribution": "scoping and nested functions"},
   "APL" : {"name": "APL", "publication_year": 1962, "contribution": "array processing"},
}

@app.get('/programming_languages')
def get_programming_languages():
    return in_memory_datastore
    

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=port)