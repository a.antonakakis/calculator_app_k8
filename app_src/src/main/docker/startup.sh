#!/bin/bash

echo "startup.sh: $(date)"

# Are you root?
if [ "$EUID" -ne 0 ]
then
    echo "startup.sh: Please run as root"
    exit 1
fi

# check if the dir /home/deployments/projects/esg-credentials-app/Backend/esg_credentials exists
if [ ! -d /home/deployments/projects/esg-credentials-app/Backend/esg_credentials ]
then
    echo "startup.sh: /home/deployments/projects/esg-credentials-app/Backend/esg_credentials does not exist"
    exit 1
fi

    # cd to the dir
    cd /home/deployments/projects/esg-credentials-app/Backend/esg_credentials

# run startup routine
echo "Build jar file of credentials"
mvn clean install -DskipTests

echo "Building credentials app and its database images"
docker build -t postgres/credentials -f Dockerfile .
docker build -t credentials/sles -f DockerJarfile .

echo "Deploying credentials app  and its database"
docker-compose up -d

echo "Done."