#!/bin/bash

# Author: Alexandros Emmanouil Antonakakis
# Description: Deletes postgresdb files on /home/deployments/credentials
# Version: 0.1

# print date and time of execution
echo "delete_db.sh: $(date)"

# Are you root?
if [ "$EUID" -ne 0 ]
then
    echo "delete_db.sh: Please run as root"
    exit 1
fi

# check if postgresdb exists
if [ ! -d /home/deployments/credentials ]
then
    echo "delete_db.sh: /home/deployments/credentials does not exist"
    exit 1
fi

# delete postgresdb
echo "delete_db.sh: deleting /home/deployments/credentials"
rm -rf /home/deployments/credentials

# check if postgresdb was deleted
if [ -d /home/deployments/credentials ]
then
    echo "delete_db.sh: /home/deployments/credentials was not deleted. Please mention this to the administrator."
    exit 1
fi

echo "delete_db.sh: /home/deployments/credentials deleted"

exit 0



