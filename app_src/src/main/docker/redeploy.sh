#!/bin/bash

# Author: Alexandros Emmanouil Antonakakis
# Description: Fetches the latest version of the project from GitLab and deploys it
# Version: 0.1

# print date and time of execution
echo "redeploy.sh: $(date)"

# Are you root?
if [ "$EUID" -ne 0 ]
then
    echo "redeploy.sh: Please run as root"
    exit 1
fi

# check if git is installed
if [ ! -x /usr/bin/git ]
then
    echo "redeploy.sh: git not installed"
    exit 1
fi

# git checkout production_branch
echo "redeploy.sh: git checkout production_branch"
(cd /home/deployments/projects/esg-credentials-app && git checkout production_branch)

# git pull
echo "redeploy.sh: git pull"
(cd /home/deployments/projects/esg-credentials-app && git pull)

# check if the project was pulled
if [ $? -ne 0 ]
then
    echo "redeploy.sh: git pull failed"
    exit 1
fi

# check if the startup and stop scripts exists
if [ ! -f /home/deployments/projects/esg-credentials-app/Backend/esg_credentials/startup.sh ]
then
    echo "redeploy.sh: /home/deployments/projects/esg-credentials-app/Backend/esg_credentials/startup.sh does not exist"
    exit 1
fi

if [ ! -f /home/deployments/projects/esg-credentials-app/Backend/esg_credentials/stop.sh ]
then
    echo "redeploy.sh: /home/deployments/projects/esg-credentials-app/Backend/esg_credentials/stop.sh does not exist"
    exit 1
fi


# stop the project
echo "redeploy.sh: stopping the project"
(cd /home/deployments/projects/esg-credentials-app/Backend/esg_credentials && ./stop.sh)

# execute startup
echo "redeploy.sh: executing startup script"
(cd /home/deployments/projects/esg-credentials-app/Backend/esg_credentials && ./startup.sh)