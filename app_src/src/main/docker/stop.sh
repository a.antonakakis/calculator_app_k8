#!/bin/bash

echo "stop.sh: $(date)"

# Are you root?
if [ "$EUID" -ne 0 ]
then
    echo "stop.sh: Please run as root"
    exit 1
fi

# check if scripts exists
if [ ! -f /home/deployments/projects/esg-credentials-app/Backend/esg_credentials/stop.sh ]
then
    echo "stop.sh: /home/deployments/projects/esg-credentials-app/Backend/esg_credentials/stop.sh does not exist"
    exit 1
fi

# execute startup script in /home/deployments/projects/esg-credentials-app/Backend/esg_credentials

(cd /home/deployments/projects/esg-credentials-app/Backend/esg_credentials && ./stop.sh)