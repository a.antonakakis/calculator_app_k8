package com.example.simple_api_calculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleApiCalculatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleApiCalculatorApplication.class, args);
	}

}
