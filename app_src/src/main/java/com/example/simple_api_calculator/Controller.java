package com.example.simple_api_calculator;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/calc")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class Controller {
@Autowired
private CalcService calcService;

@PostMapping(value = "/add", consumes = "application/json")
public Calculation add(@RequestBody Calculation calculation) {
    System.out.printf("calculation: %s\n", calculation);
    return calcService.add(calculation);
}

@PostMapping(value = "/sub", consumes = "application/json")
public Calculation subtract(@RequestBody Calculation calculation) {
    return calcService.subtract(calculation);
}

@PostMapping(value = "/mul", consumes = "application/json")
public Calculation multiply(@RequestBody Calculation calculation) {
    return calcService.multiply(calculation);
}

@PostMapping(value = "/div", consumes = "application/json")
public Calculation divide(@RequestBody Calculation calculation) {
    return calcService.divide(calculation);
}
@PostMapping(value = "/read/{id}", produces = "application/json")
public Calculation read(@PathVariable int id) {
    return calcService.read(id);
}

@PostMapping(value = "/getall", produces = "application/json")
public Iterable<Calculation> getAll() {
    return calcService.getAll();
}

@PostMapping(value = "/update", consumes = "application/json")
public Calculation update(@RequestBody Calculation calculation) {
    return calcService.update(calculation);
}

@GetMapping(value = "/delete/{id}")
public void delete(@PathVariable int id) {
    calcService.delete(id);
}

@GetMapping(value = "/clear")
public void clear() {
    calcService.clear();
}



}
