package com.example.simple_api_calculator;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CalcService {
    @Autowired
    private CalcRepository repository;

    public Calculation add(Calculation calculation) {
        calculation.setResult(calculation.getNum1() + calculation.getNum2());
        calculation.setOperator("+");
        return repository.save(calculation);
    }

    public Calculation subtract(Calculation calculation) {
        calculation.setResult(calculation.getNum1() - calculation.getNum2());
        calculation.setOperator("-");
        return repository.save(calculation);
    }

    public Calculation multiply(Calculation calculation) {
        calculation.setResult(calculation.getNum1() * calculation.getNum2());
        calculation.setOperator("*");
        return repository.save(calculation);
    }

    public Calculation divide(Calculation calculation) {
        if (calculation.getNum2() == 0)
            throw new RuntimeException("Cannot divide by 0");

        calculation.setResult(calculation.getNum1() / calculation.getNum2());
        calculation.setOperator("/");
        return repository.save(calculation);
    }

    public Calculation read(Integer id) {
        System.out.println("looking for id: " + id);

        if (id < 0)
            throw new RuntimeException("Id must be greater than 0");

        return repository.findById(id).orElseThrow(
                () -> new RuntimeException("Calculation not found")
        );

    }

public Calculation update(Calculation calculation) {
        Calculation calculationdb = read(calculation.getId());

        calculationdb.setNum1(calculation.getNum1());
        calculationdb.setNum2(calculation.getNum2());
        calculationdb.setOperator(calculation.getOperator());
        calculationdb.setResult(calculation.getResult());

        return repository.save(calculationdb);
    }

    public void delete(int id) {
        repository.deleteById(id);
    }

    public Iterable<Calculation> getAll() {
        return repository.findAll();
    }

    public void clear() {
        repository.deleteAll();
    }

}
