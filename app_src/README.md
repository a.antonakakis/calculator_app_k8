# calculator-api

A simple calculator.

## Building

- Run `build.cmd` script

- or Run `./mvnw clean package -DskipTests` and copy the jar file from `target` folder to `src/main/docker` folder

- Go to `src/main/docker` and execute `docker-compose up`

- Then you can call the api through postman or whatever tool.

## Calls

All HTTP request types for the calls, are **POST**.

```
    localhost:8080/api/calc/div
    localhost:8080/api/calc/mul
    localhost:8080/api/calc/add
    localhost:8080/api/calc/sub
    localhost:8080/api/calc/getall // fetches previous operations and results from db
```


## Example

**POST**: `localhost:8080/api/calc/div`
**BODY**:
    ```
    {
    "num1": "9",
    "num2": "8"
    }
    ```

**RESULT**:
    ```
    {
    "id": 1,
    "num1": 9.0,
    "num2": 8.0,
    "operator": "+",
    "result": 17.0
    }
    ```

## Implementing `httpexchanges` actuator

- ```agsl
  Start by adding the dependency to your pom.xml:
    		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		    </dependency> 
    ```
- ```agsl
    Add the following to your application.properties:
    management.endpoints.web.exposure.include=httpexchanges
    ```
- ```agsl
  Since the httpexchanges actuator was removed in spring 2.2 you need to create a custom actuator class which you can find in sources of this project.
  HttpTraceActuatorConfiguration.java
  ```

- ```agsl
    You can now call the actuator endpoint:
     http://localhost:8080/actuator/httpexchanges (or whatever port and address you are using via a GET request)
    ```