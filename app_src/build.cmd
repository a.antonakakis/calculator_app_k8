@ECHO OFF
setlocal EnableDelayedExpansion

set "mvn_command=mvn clean package -DskipTests=true"

@ECHO ON

@REM execute mvn command
start /B /wait %mvn_command%

@REM copy jar to docker folder
start /B /wait copy /y target\*.jar src\main\docker