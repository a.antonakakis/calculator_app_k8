# calculator_app_k8



## How to use

- Run `make` to make the scripts executable
- You can deploy with `./scripts/startall` (for now you have to `cd scripts`)
- You can delete the deployment with `./stopall`
- Access app logs with `./getapp_logs`
- Access db logs with `./getdb_logs`

## How to test

- `minikube start` to start minikube
- `minikube tunnel` to start tunnel
-  Access through post (or curl), POST 127.0.0.1:8080/api/calc/add with json body `{"num1": 1, "num2": 2}`
